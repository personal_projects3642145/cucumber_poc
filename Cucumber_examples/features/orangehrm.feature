# To run this, we have to write:
# behave features/orangehrm.feature
# in the terminal
# if we want to run every test in the folder, we use
# behave ./features

# Video 3: First example of Cucumber in Python using Selenium

Feature: OrangeHRM Logo

  Scenario: Logo presence on OrangeHRM home Page
    Given launch chrome browser
    When open orange hrm openpage
    Then verify that the logo present on page
    And close browser