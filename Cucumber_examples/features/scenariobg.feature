# To run this, we have to write:
# behave features/scenariobg.feature
# in the terminal
# if we want to run every test in the folder, we use
# behave ./features

# Video 6: Background

Feature: OrangeHRM Login

# This steps will be executed before every Scenario
  Background: common steps
    Given I launch browser
    When I open application
    And Enter valid username and password
    And click on login

  Scenario: Login to HRM Application
    Then User must login to the Dashboard page

  Scenario: Search user
    When navigate to search page
    Then search page should display

  Scenario: Advanced Search user
    When navigate to advanced search page
    Then advanced search page should display
