# To run this, we have to write:
# behave features/orangehrmlogin.feature
# in the terminal
# if we want to run every test in the folder, we use
# behave ./features

# Video 4: First example of Cucumber in Python using Step parameters
# Video 5: Scenario Outline examples
# Scenario Outline allows us to test the same Scenario with multiples possibilities

Feature: OrangeHRM Login

  Scenario: Login to OrangeHRM with valid parameters
    Given I launch Chrome browser
    When I open orange HRM Homepage
    And Enter username "admin" and password "admin123"
    And Click on login button
    Then User must successfully login to the Dashboard page

  Scenario Outline: Login to OrangeHRM with Multiple parameters
    Given I launch Chrome browser
    When I open orange HRM Homepage
    And Enter username "<username>" and password "<password>"
    And Click on login button
    Then User must successfully login to the Dashboard page

    Examples:
      | username | password |
      | admin    | admin123 |
      | admin123 | admin    |
      | adminxyz | admin123 |
      | admin    | adminxyz |