# Pagina donde veo estas pruebas
# https://www.youtube.com/watch?v=JIyvAFBx2Fw&list=PLUDwpEzHYYLsARXz1o3Ldt1FnvRbvlxsS

# Video 3: First example of Cucumber in Python using Selenium

from behave import *
from selenium import webdriver


@given(u'launch chrome browser')
def launchBrowser(context):
    PATH = "/home/fcapdeville/Temporal/Python/Selenium/chromedriver_linux64/chromedriver"
    context.driver = webdriver.Chrome(executable_path=PATH)


@when(u'open orange hrm openpage')
def openHomePage(context):
    context.driver.get("https://opensource-demo.orangehrmlive.com/")


@then(u'verify that the logo present on page')
def verifyLogo(context):
    status = context.driver.find_element_by_xpath("//div[@id='divLogo']//img").is_displayed()
    assert status is True


@then(u'close browser')
def closeBrowser(context):
    context.driver.close()