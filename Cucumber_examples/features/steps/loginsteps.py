# Pagina donde veo estas pruebas
# https://www.youtube.com/watch?v=JIyvAFBx2Fw&list=PLUDwpEzHYYLsARXz1o3Ldt1FnvRbvlxsS

# Video 4: First example of Cucumber in Python using Step parameters
# Video 5: Scenario Outline examples


from behave import *
from selenium import webdriver
import time


@given(u'I launch Chrome browser')
def step_impl(context):
    PATH = "/home/fcapdeville/Temporal/Python/Selenium/chromedriver_linux64/chromedriver"
    context.driver = webdriver.Chrome(executable_path=PATH)


@when(u'I open orange HRM Homepage')
def step_impl(context):
    context.driver.get("https://opensource-demo.orangehrmlive.com/")


@when(u'Enter username "{user}" and password "{password}"')
def step_impl(context, user, password):
    context.driver.find_element_by_id("txtUsername").send_keys(user)
    context.driver.find_element_by_id("txtPassword").send_keys(password)


@when(u'Click on login button')
def step_impl(context):
    context.driver.find_element_by_id("btnLogin").click()


@then(u'User must successfully login to the Dashboard page')
def step_impl(context):
    try:
        text = context.driver.find_element_by_xpath("//h1[contains(text(),'Dashboard')]").text
    except:
        context.driver.close()
        assert False, "Test Failed"
    if text == "Dashboard":
        context.driver.close()
        assert True, "Test Passed"
